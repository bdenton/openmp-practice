/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Synchronization.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 03/22/2013
PROJECT    : 
DESCRIPTION: Practice parallel C++ programming with OpenMP library.
///////////////////////////////////////////////////////////////////////////////*/

#include<iostream>
#include <omp.h>

int main( int argc, char *argv[] ){

  unsigned long int w = 0, x = 0, y = 0, z = 0;

#pragma omp parallel private(w,x,y,z)
  {

    // If you don't include the nowait option each loop will run sequentially.
    // Though the threads *within* each loop will still run in parallel.

    for( unsigned long int i = 0; i < 100; i++ )
      w += 1;
    
    #pragma omp barrier
    std::cout << "First loop: w = " << w << std::endl;

    for( unsigned long int j = 0; j < 100; j++ )
      x += 1;
    #pragma omp barrier
    std::cout << "Second loop: x = " << x << std::endl;

    //std::cout << std::endl << std::endl;
  
    //#pragma omp for nowait
  
    // for( unsigned long int j = 0; j < 100; j++ )
    //   y += 1;
    
    // std::cout << "Third loop: y = " << y << std::endl;
    
    // for( unsigned long int k = 0; k < 100; k++ )
    //   z += 1;
    
    // std::cout << "Fourth loop: z = " << z << std::endl;
    
  }
}

//#pragma omp for nowait

//#pragma omp barrier




//END OF FILE
